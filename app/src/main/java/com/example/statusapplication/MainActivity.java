package com.example.statusapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.statusapplication.model.Status;
import com.example.statusapplication.model.StatusData;
import com.example.statusapplication.network.ApiService;
import com.example.statusapplication.network.RetrofitClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    ProgressDialog progressDialog;
    private MyAdapter myAdapter;
    private RecyclerView myRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        /*Create handle for the RetrofitInstance interface*/
        ApiService service = RetrofitClient.getRetrofitInstance().create(ApiService.class);
        Call<Status> call = service.getAllApiStatus();
        call.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status
                    > response) {
                progressDialog.dismiss();
                Log.i("StatusApp","onResponse " + response.body().getApiDbStatus() + " " + response.body().getSiteStatus());
                Toast.makeText(MainActivity.this, "Successfully parsed API Status!", Toast.LENGTH_SHORT).show();
                loadStatusList(response.body());
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                progressDialog.dismiss();
                Log.i("StatusApp","onFailure " + t.getMessage() + "  " + call.toString());
                Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadStatusList(Status status) {

        myRecyclerView = findViewById(R.id.myRecyclerView);
        Map<String, StatusData> statusMap = status.getApiDbStatus();
        statusMap.putAll(status.getSiteStatus());
        myAdapter = new MyAdapter(statusMap, MainActivity.this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        myRecyclerView.setLayoutManager(layoutManager);
        myRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        myRecyclerView.setAdapter(myAdapter);
    }


    @Override
    public void onItemClick(String apiName, StatusData statusData) {

        Intent intent = new Intent(this, StatusDetailsActivity.class);
        intent.putExtra(StatusDetailsActivity.TITLE_TAG, apiName);
        intent.putExtra(StatusDetailsActivity.OBJECT_TAG, statusData);
        startActivity(intent);
    }

}
