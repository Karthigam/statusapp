package com.example.statusapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.statusapplication.model.StatusData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.CustomViewHolder> {


    interface OnItemClickListener{
        void onItemClick(String apiName, StatusData statusData);
    }

    private Map<String, StatusData> statusMap;
    private List<String> statusList;
    Context context;
    OnItemClickListener clickListener = null;

    public MyAdapter(Map<String, StatusData> statusMap, MainActivity context){
        this.context = context;
        this.statusMap = statusMap;
        this.statusList = new ArrayList<String>(statusMap.keySet());
        this.clickListener = context;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View myView;

        TextView apiName;
        TextView apiUrl;
        TextView apiResponseCode;

        CustomViewHolder(View itemView) {
            super(itemView);
            myView = itemView;

            apiName = myView.findViewById(R.id.statusItemName);
            apiUrl = myView.findViewById(R.id.statusItemUrl);
            apiResponseCode = myView.findViewById(R.id.statusItemResponseCode);

        }

    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_item_layout, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final String apiItem = statusList.get(position);
        holder.apiName.setText(apiItem);
        final StatusData statusData = statusMap.get(apiItem);
        if(statusData != null){
            String url = statusData.getUrl();

            if(url != null){
                holder.apiUrl.setText(context.getString(R.string.url) + ": " + url);
                holder.apiUrl.setVisibility(View.VISIBLE);
            }else{
                holder.apiUrl.setVisibility(View.GONE);
            }

            holder.apiResponseCode.setText(context.getString(R.string.response_code) + ": " + statusData.getResponseCode().toString());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(apiItem, statusData);
            }
        });

    }

    @Override
    public int getItemCount() {
        return statusList.size();
    }
}

