package com.example.statusapplication;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.statusapplication.model.StatusData;

public class StatusDetailsActivity extends AppCompatActivity {

    public static final String TITLE_TAG = "Title";
    public static final String OBJECT_TAG = "StatusObject";
    private StatusData statusData = null;
    public static final String DEFAULT = "-";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Bundle bundle = getIntent().getExtras();
        statusData = (StatusData) bundle.get(OBJECT_TAG);
        String title = (String)bundle.get(TITLE_TAG);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);

        displayDetailsView();
    }


    private void displayDetailsView(){

        String url = DEFAULT;
        if(statusData.getUrl() != null){
            url = statusData.getUrl();
        }
        ((TextView)findViewById(R.id.status_url)).setText(url);
        ((TextView)findViewById(R.id.status_reponse_code)).setText(statusData.getResponseCode().toString());
        Double time = statusData.getResponseTime();
        String stTime = DEFAULT;
        if(time != null){
            stTime = time + " " + getString(R.string.secs);
        }
        ((TextView)findViewById(R.id.status_reponse_time)).setText(stTime);
        ((TextView)findViewById(R.id.status_class)).setText(statusData.getStClass());
    }

}
