package com.example.statusapplication.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StatusData implements Serializable {

    @SerializedName("url")
    private String url;
    @SerializedName("responseCode")
    private Integer responseCode;
    @SerializedName("responseTime")
    private Double responseTime;
    @SerializedName("class")
    private String stClass;

    public StatusData(String url, Integer responseCode, Double responseTime, String stClass) {
        this.url = url;
        this.responseCode = responseCode;
        this.responseTime = responseTime;
        this.stClass = stClass;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Double getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Double responseTime) {
        this.responseTime = responseTime;
    }

    public String getStClass() {
        return stClass;
    }

    public void setStClass(String stClass) {
        this.stClass = stClass;
    }

}
