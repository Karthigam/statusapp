package com.example.statusapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Status {

    @SerializedName("APIs & DBs")
    @Expose
    private Map<String, StatusData> apiDbStatus;
    @SerializedName("Sites")
    @Expose
    private Map<String, StatusData> siteStatus;

    public Map<String, StatusData> getApiDbStatus() {
        return apiDbStatus;
    }

    public void setApiDbStatus(Map<String, StatusData> apiDbStatus) {
        this.apiDbStatus = apiDbStatus;
    }

    public Map<String, StatusData> getSiteStatus() {
        return siteStatus;
    }

    public void setSiteStatus(Map<String, StatusData> siteStatus) {
        this.siteStatus = siteStatus;
    }

}
