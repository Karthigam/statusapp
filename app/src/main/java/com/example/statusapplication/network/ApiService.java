package com.example.statusapplication.network;

import com.example.statusapplication.model.Status;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("/status")
    Call<Status> getAllApiStatus();

}
